#!/bin/bash

BUILDDIR=build
nacl-gcc \
    -B$BUILDDIR/csu/ -L$BUILDDIR -static -Wl,-T,elf_i386.x \
    -g hellow.c -o hellow \
    -Wl,-print-map > hellow.map
